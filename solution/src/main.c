#include "functions_for_main.h"
#include "main_structs.h"
#include "work_with_bmp.h"
#include "work_with_files.h"
#include <stdio.h>
#include <stdlib.h>

void destruct_malloc(struct image image, struct image rot_image){
    free(image.data);
    free(rot_image.data);
    fprintf(stderr, "%s","Memory was freed successfully.\n");
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    const char *path_to_in = argv[1];
    const char *path_to_out = argv[2];

    FILE * input_file;
    FILE * output_file;

    printing_status(read_file(&input_file, path_to_in));

    struct image old_image = {0};

    if(!from_bmp(input_file, &old_image)){
        fprintf(stderr, "%s","conversion error from bmp\n");
        free(old_image.data);
        return 1;
    }
    else fprintf(stdout, "%s","successfully conversion from bmp\n");
    fclose(input_file);

    struct image rotated_image = rotate(old_image);

    printing_status(write_file(&output_file, path_to_out));
    if(!to_bmp(output_file, &rotated_image)){
        fprintf(stderr, "%s","conversion error to bmp\n");
        destruct_malloc(old_image,rotated_image);
        return 1;
    }
    else fprintf(stdout, "%s","successfully conversion to bmp\n");
    fclose(output_file);

    destruct_malloc(old_image,rotated_image);
    fprintf(stdout, "%s","successfully!!!\n");
    return 0;
}
