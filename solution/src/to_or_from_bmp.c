#include "main_structs.h"
#include "work_with_bmp.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header read_header(FILE* in);

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

struct bmp_header read_header(FILE* in){
    struct bmp_header header;
    fread(&header, 1, sizeof(struct bmp_header), in);
    return header;
}
uint64_t padding(struct image const* img){
    return (img->width % 4);
}

void create_image(struct image* img, struct bmp_header header){
    img->width = header.biWidth;
    img->height = header.biHeight;
}



bool from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = read_header(in);
    if (!(header.bfType != 0x4D42 && header.biBitCount != 24 && (header.biSize != 40 || header.biCompression != 0 || (header.bfileSize != header.bOffBits + header.biSizeImage)))) {
        create_image(img, header);
        img->data = (struct pixel*) malloc(header.biHeight * header.biWidth * sizeof(struct pixel));
        uint64_t pad = padding(img);
        for (size_t i = 0; i < img->height; i++) {
            if(!fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in) || fseek(in, (long)pad, SEEK_CUR)){
                return false;
            }
        }
        return true;
    }
    return false;
}



bool to_bmp( FILE* out, struct image const* img ){
    struct bmp_header new_header = {
            .bfType = 0x4D42,
            .bfileSize = (img->width) * (img->height) * sizeof(struct pixel) + (img->height) * padding(img) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (img->width) * (img->height) * sizeof(struct pixel) + (img->height) * padding(img),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
    uint64_t pad = padding(img);
    fwrite(&new_header, 1, sizeof(struct bmp_header), out);
    const struct pixel nulls[] = {{0}, {0}, {0}};
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(&(img -> data)[i * img -> width], sizeof(struct pixel), img -> width, out)
            || (pad && !fwrite(nulls, 1, pad, out))) {
            return false;
        }
    }
    return true;

}
