#include "work_with_files.h"
#include <stdio.h>

char* const statuses[]={
        [OPEN_OK]="file open successfully\n",
        [READ_ERROR]="READ ERROR!\n",
        [WRITE_ERROR]="WRITE ERROR!\n"
};


enum open_status read_file(FILE** file, const char* path){
    if ((*file = fopen(path, "rb")) == NULL) return READ_ERROR;
    else return OPEN_OK;
}

enum open_status write_file(FILE** file, const char* path){
    if ((*file = fopen(path, "wb")) == NULL) return WRITE_ERROR;
    else return OPEN_OK;
}

void printing_status(enum open_status st){
    fprintf(stderr, "%s", statuses[st]);
}
