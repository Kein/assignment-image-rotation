#include <functions_for_main.h>
#include <stdint.h>
#include <stdlib.h>

uint64_t adress_of_pixels(struct image const image,size_t row, size_t column){
    return image.height * image.width-(row+1)-column*image.height;
}

struct image rotate(struct image const image){
    struct image rot_image = {
            .height = image.width,
            .width = image.height,
    };
    rot_image.data = (struct pixel*) malloc(image.height * image.width * sizeof(struct pixel));
    for (size_t row = 0; row < image.height; row++) {
        for (size_t column = 0; column <image.width; column++) {
            rot_image.data[adress_of_pixels(image,row,column)] = image.data[(row+1)*image.width-(column+1)];
        }
    }
    return rot_image;
}
