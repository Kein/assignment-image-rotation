//
// Created by fardz on 18.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FUNCTIONS_FOR_MAIN_H
#define ASSIGNMENT_IMAGE_ROTATION_FUNCTIONS_FOR_MAIN_H
#include "main_structs.h"
#include "work_with_bmp.h"
#include "work_with_files.h"
#include <stdint.h>
#include <stdio.h>

struct image rotate( struct image const image );
uint64_t padding(struct image const* image);
struct bmp_header create_header(struct image const* image);
void destruct_malloc(struct image image, struct image rot_image);


#endif //ASSIGNMENT_IMAGE_ROTATION_FUNCTIONS_FOR_MAIN_H
