//
// Created by fardz on 18.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_FILES_H
#define ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_FILES_H
#include <stdio.h>
enum open_status  {
    OPEN_OK = 0,
    READ_ERROR,
    WRITE_ERROR
};

enum open_status read_file(FILE** file, const char* path);

enum open_status write_file(FILE** file, const char* path);

void printing_status(enum open_status st);


#endif //ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_FILES_H
