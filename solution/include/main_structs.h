//
// Created by fardz on 18.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MAIN_STRUCTS_H
#define ASSIGNMENT_IMAGE_ROTATION_MAIN_STRUCTS_H
#include <stdint.h>
#include <stdio.h>


struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_MAIN_STRUCTS_H
