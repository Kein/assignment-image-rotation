//
// Created by fardz on 18.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_BMP_H
#include "main_structs.h"
#include <stdbool.h>
#include <stdio.h>
/*  deserializer   */


bool from_bmp( FILE* in, struct image* img );

/*  serializer   */


bool to_bmp( FILE* out, struct image const* img );


#endif //ASSIGNMENT_IMAGE_ROTATION_WORK_WITH_BMP_H
